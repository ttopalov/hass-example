# hass-example

# Example Home Assistant setup using docker-compose and gitlab-ci

When executed, this will spin up the following services:
1. Postgres server - by default hass uses sqlite for log book, which gets very slow in a few weeks. This is why we use postres instead
2. Mqtt server (mosquitto). Not much to be said here.
3. Homeassistant - the cool stuff.
4. Letsencrypt-equipped nginx reverse proxy. This one does some magick. Not only will it redirect http to https and automatically get ssl certificates. It also includes a scan bot protection. This means the reverse proxy is configured to only serve home assiatant if you get the domain right. If you don't, you will get an ssl handshake rejection preventing your correct hostname to leak along with the certificate.

# What you need to change
Very little, at least initially. You need to set your postgres password in compose and configuration.yaml.
It is recommended that you take a look at all configuration files and understand what they're doing. Ater all, they are quite minimal.
The are example MQTT sensors in the configuration you probably don't need.
You need a domain (subdomain) and put it in sites.conf in order for the proxy magic to work. If you don't, you can just comment the whole letsencrypt service in docker-compose and access your hass at port 8123.
To set up CI, you need to register a runner and change the tag in gitlab-ci to match it. Make sure it has docker and docker-compose, the gitlab-runner user can execute docker commands and owns /etc/hass.

# Other perks
This compose uses only multiarch docker images, meaning it can run on both arm64 and amd64 hosts.
